# Industria 4.0


```mermaid
graph TD;

    A{{Industria 4.0}}-->B(Historia);
    subgraph " "
    B--1784-->C(Industria 1.0);
    B--1870-->D(Industria 2.0);
    B--1969-->E(Industria 3.0);
    B--ACTUAL-->F(Industria 4.0);
    
    C--consistio-->G(Produccion Mecanica);
    D--consistio-->H(Produccion de Carros);
    E--consistio-->I(Produccion Automatizada);
    F--consistio-->J(Produccion Inteligente);
    end

    
    A-->T(Elementos);
    subgraph " "
    T-->U(Big Data <br>Simulacion<br>Fabricación aditiva<br>Ciberseguridad<br>Cloud computing<br>Internet de las Cosas<br>Sistemas  ciberfísicos y robótica<br>Integración<br>Realidad aumentada);
    end
    
    A(Industria 4.0)-->K(Cambios);
    subgraph " "
    K-->L(Integracion de TICs);
    K-->M(Empresas de manufactura a Empresas de TICs);
    K-->N(Nuevos Paradigmas y Tecnologias);
    K-->O(Nuevas Culturas Digitales);
    
    L--aporta-->P(Fabrica de Automoviles<br>Manejo de Inventario<br>Entrega de Productos);
    M--aporta-->Q(Electrodomesticos Modernos<br>Industria automovil moderna);
    N--aporta-->R(Velocidad en los negocios<br>Negocios Basados en Plataformas<br>Internet de las Cosas<br>BIG DATA e Inteligencia Artificial);
    O--aporta-->S(Phono Sapiens<br>Youtubers<br>E-SPORTS);

    L--efecto-->V(Reduccion de puestos de trabajos<br>Aparicion de nuevas profesiones);
    M--efecto-->W(Existen menos diferencias entre industrias);
    N--efecto-->X(Empresas mas eficientes);
    O--efecto-->Y(Competicion y Oportunidades);
    end
    
    
    style A stroke:#FF0000,stroke-width:3px

    style B stroke:#335FE9,stroke-width:3px
    style T stroke:#335FE9,stroke-width:3px
    style K stroke:#335FE9,stroke-width:3px

    style C stroke:#00F7F7,stroke-width:3px
    style D stroke:#00F7F7,stroke-width:3px
    style E stroke:#00F7F7,stroke-width:3px
    style F stroke:#00F7F7,stroke-width:3px
    style L stroke:#00F7F7,stroke-width:3px
    style M stroke:#00F7F7,stroke-width:3px
    style N stroke:#00F7F7,stroke-width:3px
    style O stroke:#00F7F7,stroke-width:3px
    style U stroke:#00F7F7,stroke-width:3px
    
```

# Mexico y la Industria 4.0 

```mermaid
graph TD;

A--Logros-->F(Se han puesto en marcha iniciativas y proyectos para introducir la <br> Industria 4.0 en nuestro país. México ha iniciado un proceso de incorporación a la Industria 4.0<br> en diferentes niveles, que van desde educativo, gerencial, de políticas públicas, industrial, entre otros.);
subgraph " "
F--Ejemplo-->G(Sin Llave es una startup creada en México que logró una oportunidad <br> con Airbnb. Se trata de una aplicación que permite acceder a la propiedad <br> sin la necesidad de contar con una llave física, solamente con nuestro Smartphone);
end

A--Cambios a futuro-->D(La llegada de la Industria 4.0 en México representará un cambio de paradigma <br >en los próximos años que brindará grandes oportunidades para el sector manufacturero del país.);
subgraph " "
D--Se espera-->E(Mayor innovacion<br>Mayor uso de tecnologias para la fabricacion<br>Integracion a cadenas globales de valor);
end

A{{Mexico y la Industria 4.0}}--Consiste-->B(Implementación de la automatización y el intercambio <br> de datos dentro del sector manufacturero del país.);
subgraph " "
B--Tecnologias tales como-->C(Internet de las Cosas <br>Impresión 3D<br>Computación Cuántica<br>Nanotecnologia<br>Cloud Computing y Big Data<br>Inteligencia Artificial<br>Automatizacion);
end
    
    style A stroke:#A70ACA,stroke-width:3px

    style F stroke:#650CCF,stroke-width:3px
    style D stroke:#650CCF,stroke-width:3px
    style B stroke:#650CCF,stroke-width:3px

    style C stroke:#440F52,stroke-width:3px
    style E stroke:#440F52,stroke-width:3px
    style G stroke:#440F52,stroke-width:3px

    
```
